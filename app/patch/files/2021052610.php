<?php
@set_time_limit(0);
defined('iPHP') or require(dirname(__FILE__) . '/../../../iCMS.php');

// patch::$check_login = 0;//debug

$title = '更新应用表';
return patch::upgrade(function () {
  $msg = '清除文章应用测试时遗留字段数据<iCMS>';
  AppsModel::where(['id' => 1])->update(['fields' => '']);
  $msg .= '清除所有应用测试时遗留路由数据<iCMS>';
  AppsModel::update(['route' => '']);
  Apps::cache();
  return $msg;
});
